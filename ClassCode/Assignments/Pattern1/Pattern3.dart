import 'dart:io';

void main() {
    int n = 5;
    int half=(n/2).ceil();
    for (int i = 1; i <= n; i++) {
	if(i<half){
        	for (int j = 1; j <= i; j++) {
            		stdout.write("* ");
        	}
        	print("");
	}else{
		for(int j=i;j<=n;j++){
			stdout.write("* ");
		}
		print("");
	}
    }
}

