import 'dart:io';

void main() {
    int n = 5;

    for (int i = 1; i <= n; i++) {
        for (int j = n-i; j>= 1; j--) {
            stdout.write(" ");
        }
        for (int j = 1; j <= i; j++) {
            stdout.write("* ");
        }
        print("");
    }
}

