class Demo{

        int? _x;
        String? str;
        double? _sal;

        Demo(this._x,this.str,this._sal);

        int? get getX => _x;
        
//OR 	get getX => x;

        double? get getSal => _sal;
//OR 	get getsal => _sal;

	void Disp(){
		print(_x);
		print(str);
		print(_sal);
	}
    
}
