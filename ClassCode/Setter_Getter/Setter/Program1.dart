class Demo{

        int? _x;
        String str;
        double _sal;

        Demo(this._x,this.str,this._sal);
/*
	//FIRST APPROCH:
 	void setX(int x){
		_x = x;
	}

	void setName(String name){
		str = name;
	}

	void setSal(double sal){
		_sal = sal;
	}
*/
	
/*	//SECOND APPROCH:

	set setX(int x){
		_x = x;
	}
	
	set setName(String name){
		str = name;
	}

	set setSal(double sal){
		_sal = sal;
	}
*/
	//THIRD APPROCH:

	set setX(int x) => _x = x;
	set setName(String name) => str = name;
	set setSal(double sal) => _sal = sal;

	void printdata(){
		print(_x);
		print(str);
		print(_sal);
	}	
}
