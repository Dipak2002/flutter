class Parent{

	int x = 10;
	String str = "Surname";

	void dispData(){
		print("In parent ");
	}
}
class Child extends Parent{

}
void main(){

	Child obj = new Child();
	print(obj.x);
	obj.dispData();
}
