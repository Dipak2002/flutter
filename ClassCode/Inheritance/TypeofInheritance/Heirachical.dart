class Parent{

	Parent(){
		print("in parent constructor");
	}
}
class Child extends Parent{

	Child(){
		print("In child constructor");
	}
}

class Child1 extends Parent{

	Child1(){

		print("In second child constructor");
	}
}

void main(){

	Parent obj = new Child();
	Child obj2 = new Child();
	Child1 obj3 = new Child1();
}
