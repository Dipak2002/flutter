class Parent{

        Parent(){
                print("In parent Constructor");
        }

	call(){ 	//this method are use to make the class la reprasent karnare variable callable karnyasathi.{eg.super() this are the class representive and to make this variable as callable call() method are used};
		print("In call method");
	}
}

class Child extends Parent{

        Child(){
                super();
                print("child constructor");
        }
}

void main(){

        Child obj = new Child();

	obj();//callable object karnyasathi call method write keli ahe.
}
