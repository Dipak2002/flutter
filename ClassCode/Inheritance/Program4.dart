class Parent{

	int x = 1;
	String str1 = "Dipak";

	get getX => x;
	get getstr1 => str1;
}

class Child extends Parent{

	int y = 20;
	String str2 = "Ankit";

	get getY => y;
	get getstr2 => str2;

}

void main(){

	Child obj = new Child();
	print(obj.getX);
	print(obj.getstr1);

	print(obj.getY);
	print(obj.getstr2);
}
