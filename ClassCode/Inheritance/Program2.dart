class Parent{

	int x = 10;
	String str = "name";

	void parentMethod(){
		print(x);
		print(str);
	}
}

class Child extends Parent{

	int y = 20;
	String str2 = "data";

	void childMethod(){
		print(y);
		print(str2);
	}
}

void main(){

	Parent obj = new Parent();
	print(obj.y);
	print(obj.str2);
	obj.childMethod();
}
