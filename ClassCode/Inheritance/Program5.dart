class Parent{

	int x = 10;
	String str1 = "Dipak";

	void parentMethod(){
		print(x);
		print(str1);
	}
}

class Child extends Parent{

	int x = 20;
	String str2 = "Sapate";
	
	void ChildMethod(){
		print(x);
		print(str2);
	}
}

void main(){

	Child obj1 = new Child();
	print(obj1.x);
	print(obj1.str1);
	obj1.parentMethod();

	print(obj1.x);
	print(obj1.str2);
	obj1.ChildMethod();

	Parent obj2 = new Parent();
	
	print(obj2.x);
        print(obj2.str1);
        obj2.parentMethod();
}


