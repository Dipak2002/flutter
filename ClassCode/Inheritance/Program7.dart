class Parent{

	Parent(){
		print("In parent Constructor");
	}
}

class Child extends Parent{

	Child(){
		super();
		print("child constructor");
	}
}

void main(){

	Child obj = new Child();
}
