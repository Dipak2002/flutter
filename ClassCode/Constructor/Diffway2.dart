class Company{

	int? empCount;
	String? cmpName;

	Company(this.empCount,[this.cmpName = "Binecaps"]);

	void PrintData(){
		print(empCount);
		print(cmpName);
	}
}
void main(){

	Company obj1 = new Company(23);
	Company obj2 = new Company(30, "Incubator");

	obj1.PrintData();
	obj2.PrintData();
}
