class Company{

        int? empCount;
        String? cmpName;

        Company({this.empCount,this.cmpName});

        void Printdata(){
                print(empCount);
                print(cmpName);
        }
}

void main(){

        Company obj1 = new Company(empCount:25,cmpName:"veritas");
        Company obj2 = new Company(cmpName:"Incubator",empCount:500);

        obj1.Printdata();
        obj2.Printdata();

}
