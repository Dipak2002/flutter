class Company{

	int? empCount;
	String? cmpName;

	Company(this.empCount,this.cmpName);//STANDERD WAY OF WRITING CONSTRUCTOR

	void cmpInfo(){
		print(empCount);
		print(cmpName);
	}
}

void main(){

	Company obj = new Company(100,"c2w");
	
	Company obj2 = new Company(200,"Pubmatic");

	obj.cmpInfo();

	obj2.cmpInfo();

}
