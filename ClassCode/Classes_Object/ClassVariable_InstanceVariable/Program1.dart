class Company{

	int empCount = 500;
	String cName = "Google";
	String location = "Pune";
	
	void cmpInfo(){
		print(empCount);
		print(cName);
		print(location);
	}
}

void main(){

	Company obj1 = new Company();

	Company obj2 = Company();

	Company().cmpInfo();

	new Company().cmpInfo();      //one time use hoto because we not store a value.

	obj2.cmpInfo();
}
