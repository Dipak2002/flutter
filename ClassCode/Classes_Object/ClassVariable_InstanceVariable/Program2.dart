class Employee{

        int empId = 500;
        String empName = "Dipak";
        double sal = 1.5;

        void empInfo(){
                print(empId);
                print(empName);
                print(sal);
        }
}

void main(){

        Employee obj1 = new Employee();
	obj1.empInfo();

        Employee obj2 = Employee();
	obj2.empInfo();

}
